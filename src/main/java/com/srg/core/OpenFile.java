package com.srg.core;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by administrator on 2/13/15.
 */
public class OpenFile extends AbstractFilesCommand implements Command{

    private File file = null;

    public OpenFile(String f){

        if (isOkFile(f))
            file = new File(path.getAbsolutePath() + "//" + f);

    }

    /**
     * are rol de a deschide fisiere cu aplicatiile default din system
     */
    @Override
    public void execute() {

        if (file != null)
            try {
                Desktop.getDesktop().open(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    /**
     *
     * verifica daca f este null sau daca nu este fisier si genereaza o eroare
     * cu mesajul indicat
     *
     * @param f reprezinta numele fisierului
     * @return intoarce true daca este gasit fisierul si false in alte cazuri
     */
    private boolean isOkFile(String f) {
        if (f == null || !isFile(f)){
            try {
                throw new MyException("Numele fisierului nu este indicat sau nu exista sau este directoriu");
            } catch (MyException e) {
                e.printStackTrace();
            }
            return false;
        }
        return true;
    }

    /**
     * verifica daca este fisier
     * @param f numele fisierului
     * @return returneaza true daca este adevarat si false altfel
     */
    private boolean isFile(String f){
       return (new File(path.getAbsolutePath() + "//" + f).isFile());
    }
}
