package com.srg.core;

import java.io.File;

import static com.srg.core.Commands.ERROR;

/**
 * Created by administrator on 2/13/15.
 *
 */
public class ListFiles extends AbstractFilesCommand implements Command{

    public ListFiles(String folder) {
        super.folder = folder;
    }

    @Override
    public void execute() {
        int code = isFolder(folder);

        if (code == -2)
            show(curentFolderFiles());

        else if (code == -1){
            System.err.println(ERROR);
        }
        else
            show(curentFolderFiles(getFolderPath()));
    }

    /**
     * afiseaza fisierele din lista paths care sunt de tipul type
     * @param paths lista de fisiere
     */
    private void show(File [] paths){

        for (File file : paths) {
            if (file.toString().endsWith(type)){
                System.out.println(file.getName().toString());

            }
        }
        System.out.println();
    }

    /**
     *
     * @return - lista de fisiere din directoriu path
     */
    private File[] curentFolderFiles(){
        return  path.listFiles();
    }

    /**
     *
     * @param files - folderul care va fi root
     * @return lista de fisiere din directoriu path
     */
    private File[] curentFolderFiles(File files){
        return  files.listFiles();
    }

}
