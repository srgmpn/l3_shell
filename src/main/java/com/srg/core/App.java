package com.srg.core;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by administrator on 5/9/15.
 */
public class App {

    private HashMap<String, Commands> commands;
    private String folder;
    private String cmd;
    private static final String ERROR = "Command error !";

    public App(){
        commands = new HashMap<String, Commands>();

        //seteaza in map keile string si valoarile de tip enum Commands
        for (Commands command : Commands.values()){
            commands.put(command.name().toUpperCase(), command);
        }
        cmd = "";
    }

    /**
     *cere utilizatorului sa introduca tipul fisierelor
     * cu care va lucra
     * si le verifica - daca este audio
     * @param in - obiect scaner
     */
    private void initFilesType(Scanner in){
        String type = null;
        while (true){
            System.out.print("\nSelect type files : ");
            type = in.nextLine();

            if (RegexV.validate(type, "type") || (type.length() == 0)){
                break;
            }
        }

        AbstractFilesCommand.type = type;
    }

    private Commands getCommand(String key){
        Commands command = commands.get(key.toUpperCase());
        return ((command == null)? Commands.ERROR : command);
    }

    /**
     * parseaza comanda luata de la client cmd
     * si seteaza cmd care va fi selectata din map
     * si fisierul sau mapa asupra careia se face vreo operatie
     * @param cmds
     */

    private void parseCmd(String cmds){
        String [] list = cmds.split(" ");
        cmd = list[0];
        if (list.length > 1)
            folder = list[1];
    }

    /**
     * executa commanda in dependenta de
     * comanda ceruta
     * @param cmd - comanda parsata
     */
    private void executeOperation(String cmd){
        switch (getCommand(cmd.toUpperCase())){
            case CD:
                CommandFactory.createCommand(Commands.CD, folder).execute();
                break;
            case LS:
                CommandFactory.createCommand(Commands.LS, folder).execute();
                break;
            case OPF:
                CommandFactory.createCommand(Commands.OPF, folder).execute();
                break;
            case SRCH:
                CommandFactory.createCommand(Commands.SRCH, folder).execute();
                break;
            case ERROR:
                System.err.println(ERROR);
                break;
            default:
                break;
        }
        folder = null;
    }

    /**
     * functia principala care preia comenzile
     */
    public void start(){
        Scanner in = new Scanner(System.in);
        this.initFilesType(in);

        while ( Commands.EXIT != getCommand(cmd.toUpperCase()) ){

            System.out.print("<cmd>~$ ");
            cmd = in.nextLine();
            if (RegexV.validate(cmd, "cmd")){

                this.parseCmd(cmd);
                executeOperation(cmd);

            } else {
                try {
                    throw new MyException("Comanda gresit");
                } catch (MyException e) {
                    e.printStackTrace();
                }
            }

        }

        System.err.println("Program finished. ");
    }

    public static void main(String[] args) {
        new App().start();
    }
}
