package com.srg.core;

import java.io.File;

import static com.srg.core.Commands.ERROR;

/**
 * Created by administrator on 2/13/15.
 */
public class OpenDIrectory extends AbstractFilesCommand implements Command {

    public OpenDIrectory(String folder) {
        super.folder = folder;
    }

    @Override
    public void execute() {

        if (isFolder(folder) == 0)
            updatePath();
        else
            System.err.println(ERROR);
    }

    /**
     *
     * @return numarul de pasi care trebuie facuti inapoi
     */
    private int parseFolder(String f){
        return f.split("/").length;
    }

    /**
     * face update la path prin eliminare din sir a mapelor daca se fac pasi
     * inapoi, si se verifica sa nu se faca prea multi pasi inapoi
     */
    private void updatePath(){

        if (isTruebackPath()){

                int back = parseFolder(folder);
                String p = path.getAbsolutePath();

                //eliminam din path pasii facuti inapoi
                while (back != 0) {

                    int index = p.lastIndexOf("/");
                    p = p.substring(0, index);
                    --back;
                }

                path = new File(p);

        } else {
            path = getFolderPath();
        }
    }

    /**
     * se verifica daca exista pasii inapoi
     * si daca se poate de realizat pasii inapoi
     *
     * @return true daca exista si se poate, altfel false
     */
    public boolean isTruebackPath(){

        if (folder.contains("../") && isTrueRelation())
            return true;

        return false;
    }

    /**
     * se verifica daca nr de pasi inapoi nu
     * este mai mare decit nr de mape din path
     * @return true daca este mai mic altfel false
     */
    private boolean isTrueRelation(){
        return ( parseFolder(folder) <
                (parseFolder(path.getAbsolutePath().toString()) - 1)  );
    }
}
