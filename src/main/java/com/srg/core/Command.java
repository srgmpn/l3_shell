package com.srg.core;

/**
 * Created by administrator on 5/9/15.
 */
public interface Command {
    public void execute();
}
