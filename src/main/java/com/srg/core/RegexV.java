package com.srg.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by administrator on 5/9/15.
 */
public class RegexV {
    private static Pattern pattern;
    private static Matcher matcher;

    private static final String CMD_PATTERN = "^(exit|(ls|(ls \\w((\\w|\\W)*)))|" +
                                           "((cd|srch|opf) (\\w|\\W)*))$";

    private static final String AUDIOFILE_PATTERN = "^(mp3|wav|wma|wv|msv|mmf|m4p|m4a|gsm|dvf)$";

    /**
     * Verficare s
     * si seteaza pattern dupa tipul de date care vine
     */
    public static void setPatternString(String s){
        if (s.equals("cmd")){
            pattern = Pattern.compile(CMD_PATTERN);

        } else {
            pattern = Pattern.compile(AUDIOFILE_PATTERN);

        }
    }

    /**
     * Validate hex with regular expression
     *
     * @param hex     *            hex for validation
     * @return true valid hex, false invalid hex
     */
    public static boolean validate(final String hex, String type) {
        setPatternString(type);
        matcher = pattern.matcher(hex);
        return matcher.matches();
    }
}
