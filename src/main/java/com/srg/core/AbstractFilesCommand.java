package com.srg.core;

import java.io.File;

/**
 * Created by administrator on 5/9/15.
 */
public abstract class AbstractFilesCommand {
    protected static File path = new File(".");
    public static String type;
    protected String folder;


    /**
     *
     * @param folder - numele folder-ului care trebuie de verificat
     * @return retuneaza false daca folder nu este folder sau daca folder este null,
     *          altfel se returneaza true
     */
    protected int isFolder(String folder){

        if (folder == null)
            return -2;

        if (!getFolderPath().isDirectory())
            return -1;

        return 0;
    }

    protected File getFolderPath(){
        return ( new File(path.getAbsolutePath() + "//" + folder) );
    }
}
