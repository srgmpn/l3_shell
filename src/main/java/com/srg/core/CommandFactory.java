package com.srg.core;

/**
 * Created by administrator on 5/9/15.
 */
public class CommandFactory {

    private CommandFactory(){}

    public static Command createCommand(Commands cmd, String folder){

        switch (cmd){
            case CD:
                return new OpenDIrectory(folder);

            case LS:
                return new ListFiles(folder);

            case OPF:
                return new OpenFile(folder);

            case SRCH:
                return new SearchFile(folder);

        }
        return null;
    }

}
