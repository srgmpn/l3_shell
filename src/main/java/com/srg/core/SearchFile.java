package com.srg.core;

import java.io.File;

/**
 * Created by administrator on 2/13/15.
 */
public class SearchFile extends AbstractFilesCommand implements Command {

    private String name;
    private boolean b;

    public SearchFile(String name) {
        this.name = name;
        b = false;
    }

    @Override
    public void execute() {

        searchFile(path);

        if (!b)
            try {
                throw new MyException("Fisierul nu exista in directoriu si in subdirectoriile lui!");
            } catch (MyException e) {
                e.printStackTrace();
            }

    }

    /**
     * parcurge recursiv toate folderele si cauta fisierul cu numele name
     * si afiseaza calea absoluta catre el daca este gasit
     * @param path - este directoriu din care se incepe cautarea (directoriu curent)
     */
    private  void searchFile(File path){

        File [] list = path.listFiles();

        for (File file : list) {
            if (file.isDirectory()) {

                searchFile(file);

            } else {

                if ( file.getName().equals(name) || (file.getName().startsWith(name + ".")) ) {
                    System.out.println(file.getAbsolutePath().toString());
                    b = true;
                }
            }
        }
    }
}
