package com.srg.core;

/**
 * Created by administrator on 2/13/15.
 */
public class MyException extends  Exception{
    private String msg;

    public MyException(){}

    public MyException(String msg){
        super(msg);
    }

}
