SHELL (fisiere (audio)) - Arhitectura proiect
(Aplicatia a fost realizata sa prelucreze doar commenzi Linux)

1. Mediu de dezvoltare
2. Implementare
3. Git
4. Mod de utilizare

1. Mediu de dezvoltare
    - Limbajul de programare Java 1.8
    - Mediu de dezvoltare IntelliJ IDEA
    - Maven 3.0.5


2. Implementare
     Arhitectura aplicatiei
        Clasa App este clasa care se ocupa cu primirea comenzilor de la consola
        unde ia comanda dupa care o verifica dupa o expresie regulata, este apelata functia
        statica RegexV.validate(), comanda este parsata si verificara apoi este transmisa ca
        parametru functiei care executa commanda, este implementat patternul Command si patternul
        factory method. Fiecare comanda este implementata in clasa aparte, cd - OpenDIrectory, ls -
        ListFiles, opf - OpenFile, srch - SearchFile, toate extind clasa abstracta AbstractFilesCommand
        si implementeaza interfata Command, fiecare commanda este realizata in dependenta rezultatul
        comenzii anterioare, rezultatul pastindu-se in varaiabila path a clasei AbstractFilesCommand.
        Tastarea commenzilor eronata se vor indica prin Eroare "ERROR", "Comanda gresita", sau generarea unei exceptii


3. Git
    Pentru version control, am folosit git.

4. Mod de utilizare
    Se lanseaza .jar-ul apoi se cere de introdus tipul fisierelor cu care se va lucra, daca nu se
    introduce nimic si se apasa enter atunci va vedea orice tip de fisier, apoi se asteapta
    introducerea unei commenzi:
    ls - listeaza fisierele si foldere din directoriu curent
    ls <folder> - listeaza fisierele fin folder-ul folder daca exista
    cd <folder> - avanseaza in folder (cd ../ - un pas inapoi)
    srch <file.type> - face cautarea fisierul file.type incepind cu directoriu curent si recursiv in toate
    opf <file.type> - deschide fisierul file.type cu ajutorul aplicatiei de sistem
